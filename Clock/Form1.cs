﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;
using System.Timers;

namespace Clock
{
    public partial class Form1 : Form
    {
        private System.Timers.Timer timer;
        SoundPlayer player = new SoundPlayer();
        private string msg;
        private bool clockPlaying = false;

        string pathRootFolder = @"C:\AlarmClockFiles";
        string timeFile = @"C:\AlarmClockFiles\timeFile.txt";

        public Form1()
        {
            InitializeComponent();


            if (File.Exists(timeFile))
            {
                // read time from file
                string timeFromFile = File.ReadAllText(timeFile);
                Console.WriteLine("raw: " + timeFromFile);

                // convert string to datetime and give value to dateTimePicker
                DateTime dtnew = Convert.ToDateTime(timeFromFile);
                Console.WriteLine("coverted: " + dtnew);

                dateTimePicker.Value = dtnew;
            }
            else
            {
                Console.WriteLine("file not exist");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            timer = new System.Timers.Timer();
            timer.Interval = 1000;
            timer.Elapsed += Timer_Elapsed;
        }

        public void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            DateTime currentTime = DateTime.Now;
            DateTime userTime = dateTimePicker.Value;

            if (currentTime.Hour == userTime.Hour && currentTime.Minute == userTime.Minute &&
                currentTime.Second == userTime.Second)
            {
                timer.Stop();

                // play sound and loop it
                player.SoundLocation = @"C:\Windows\media\Alarm01.wav";
                player.PlayLooping();
                clockPlaying = true;
            }

            if (clockPlaying == true)
            {
                OpenPopup(msg);
            }
        }

        private void btnStart_Click_1(object sender, EventArgs e)
        {
            if (Directory.Exists(pathRootFolder))
            {
                CreateTimeFile();
            }
            else
            {
                Directory.CreateDirectory(pathRootFolder);
                CreateTimeFile();
            }

            // start timer and change label
            timer.Start();
            lblStatus.Text = "Running...";

            AlarmMessageConfig();

        }

        private void btnStop_Click_1(object sender, EventArgs e)
        {
            // stop timer, stop player, and change label
            timer.Stop();
            player.Stop();
            lblStatus.Text = "Stopped";
            clockPlaying = false;
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
        }

        private void dateTimePicker_ValueChanged(object sender, EventArgs e)
        {
        }

        // create txt file with time from dateTimePicker
        private void CreateTimeFile()
        {
            File.WriteAllText(timeFile, dateTimePicker.Value.ToString());
            Console.WriteLine("Time saved to file: " + dateTimePicker.Value);
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
        }

        private void OpenPopup(string txt)
        {
            notifyIcon1.Icon = SystemIcons.Information;
            notifyIcon1.BalloonTipTitle = "Alarm";
            notifyIcon1.BalloonTipText = txt;
            notifyIcon1.BalloonTipIcon = ToolTipIcon.Info;
            notifyIcon1.ShowBalloonTip(30000);
            notifyIcon1.Visible = true;
        }

        private void AlarmMessageConfig()
        {
            if (richTextBox1.Text != String.Empty)
            {
                msg = richTextBox1.Text;
            }
            else
            {
                string defaultMsg = "Hello, this is your alarm.";

                msg = defaultMsg;
            }
        }

    }
}